<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Category;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'orders';


    public function product()
    {
        return $this->hasMany('App\Product', 'product_id');
    }


    protected $fillable = [
        'product_id',
        'category_id',
        'qty',
        'created_at',
        'updated_at'
    ];

    public static function group_by_category($request)
    {
        $sql =
            "SELECT c.category_name AS category, COUNT(*) as orders  FROM orders o
             JOIN (categories c)
             ON (c.id = o.category_id)
             GROUP BY c.category_name
             ORDER BY orders DESC";
        $orders = DB::select(DB::raw($sql));
        return $orders;
    }

    public static function group_by_products($request)
    {
        $from = Carbon::parse($request->from)->format('Y-m-d');
        $to = Carbon::parse($request->to)->format('Y-m-d');
        $category_id = $request->category_id;
         $orders = Order::selectRaw(
            'products.name, COUNT(orders.id) as orders, COUNT(orders.qty) AS qty, orders.created_at'
        )
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->whereBetween('orders.created_at', [$from, $to])
            ->where('orders.category_id', '=', $category_id)
            ->groupBy('products.name')
            ->orderBy('orders', 'DESC')
            ->get();
        return $orders;
    }
}
