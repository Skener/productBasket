<?php

namespace App\Http\Controllers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $order = Order::create(
            [
                'product_id' => $request->products,
                'category_id' => $request->categories,
                'qty' => $request->qty,
            ]
        );
        Session::flash('success', 'Order saved!');

        return redirect('/');
    }

    public function view(Request $request)
    {
        if ('default' == $request->category_id):

            $orders = Order::group_by_category($request);

        else:

            $orders = Order::group_by_products($request);

        endif;

        return json_encode($orders) ?? 'No products';
    }
}
