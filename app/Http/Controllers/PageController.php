<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();
        $categories = Category::all();

        return view('frontPage', ['products' => $products, 'categories' => $categories]);
    }


    public function category_products(Request $request)
    {
        $category_products = Product::where('category_id', $request->category_id)->get();
        return json_encode($category_products);
    }

}
