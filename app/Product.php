<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Order;
use Illuminate\Support\Carbon;

class Product extends Model
{

    protected $table = 'products';

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    protected $dates = [
        'deleted_at',
        'created_at',
    ];

    protected $fillable = [
        'name',
        'price',
        'category_id'
    ];


}

