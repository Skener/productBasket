<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Order;


class Category extends Model
{
    public function products()
    {
        return $this->hasMany('App\Product', 'category_id', 'id');
    }


    protected $fillable = [
        'category_name',
        'created_at',
        'updated_at'
    ];
}
