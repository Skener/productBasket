@extends('layouts.app')
@section('content')
    <div class="row">
        <p></p>
        <div class="col-lg-4 form-group">
            <div class="panel-default">
                <p class="panel-heading">Make your order</p>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Session::has('Success'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('Success')}}
                    </div>
                @endif
                <p></p>
                <form action="{{route('order.store')}}" method="post">
                    {{csrf_field ()}}
                    <select name="categories" id="category" class="form-control form-control-sm">
                        <option value="default" selected>Select Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}"> {{$category->category_name}}</option>
                        @endforeach
                    </select>
                    <div id="products-block">
                        <select name="products" id="category-products">
                            <option value="default" selected></option>
                            @if ($products)
                                @foreach($products as $product)
                                    Products:
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                        </select>
                        <p>Quantity:<input class="form-control" type="text" name="qty" value="1" maxlength="2" size="2"
                                           autocomplete="off" required></p>
                        @endif
                        <p></p>
                        <button class="btn btn-secondary">Make order</button>
                    </div>
                </form>
            </div>
        </div>
        <p></p>
        <div class="col-lg-4 form-group">
            <div class="panel-default">
                <p class="header"> View order information</p>
                <form action="" method="get">
                    {{csrf_field ()}}
                    <p></p>
                    <div>
                        <select name="categories" id="categor" class="form-control form-control-sm">
                            <option value="default" selected>Select Category</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"> {{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <p>Start Date: <input type='text' class="form-control small-1" data-provide="datepicker"
                                              id="datepickerFrom"
                                              name="datepickerFrom"
                                              autocomplete="off" value="{{old('datepickerFrom')}}">
                            End Date: <input type='text' class="form-control small-1" data-provide="datepicker"
                                             id="datepickerTo"
                                             name="datepickerTo"
                                             autocomplete="off" value="{{old('datepickerTo')}}">
                        </p>
                    </div>
                    <button class="btn btn-secondary" id="btn-order">View Order Details</button>
                </form>
                <p></p>
                <table class="table table-hover table-bordered table-responsive-sm ">
                    <tr>
                        <thead>
                        <th>
                            Total Orders
                        </th>
                        <th>
                            Product Name
                        </th>
                        <th>
                            Quantity
                        </th>

                        <th>
                            Category
                        </th>
                        <th>
                            Careated_at
                        </th>
                        </thead>
                    </tr>
                    <tbody id="table-body">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var list = $("#category-products");
            var block = $("#products-block");
            block.css('display', 'none');
            $("#category").change(function () {
                block.css('display', 'block');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('categories.products') }}?category_id=" + $(this).val(),
                    method: 'GET',
                    success: function (data) {
                        list.empty();
                        var json = JSON.parse(data);
                        var products = json.forEach(function (product) {
                            var option = `<option value="${product.id}">${product.name}</option>`;
                            list.append(option);
                        });
                    }
                });
            });

            $("#btn-order").click(function (e) {
                e.preventDefault();
                var fromVal = $("#datepickerFrom").val();
                var toVal = $("#datepickerTo").val();
                var cat = $('#categor').find(':selected').val();
                var table_body = $('#table-body');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "{{ route('order.view') }}",
                    method: 'GET',
                    data: {
                        from: fromVal,
                        to: toVal,
                        category_id: cat
                    },
                    success: function (orders) {
                        table_body.empty();
                        var json = JSON.parse(orders);
                        var items = json.forEach(function (order) {
                            var row = `
                            <tr>
                            <td>${order.orders}</td>
                            <td>${order.name}<td/>
                            <td>${order.qty}<td/>
                            <td>${order.category}<td/>
                            <td>${order.created_at}<td/>
                            </tr>`;
                            table_body.append(row);
                        });
                    }
                });
            });
            $('#datepickerFrom').datepicker({
                autoclose: true,
                todayBtn: 'linked',
                format: 'YYYY-MM-DD HH:mm:ss',
            });

            $('#datepickerTo').datepicker({
                autoclose: true,
                todayBtn: 'linked',
                format: 'YYYY-MM-DD HH:mm:ss',
            });
        });
    </script>
@endsection
