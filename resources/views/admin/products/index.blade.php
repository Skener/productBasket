@extends ('layouts.app')
@section('content')
    <div class="panel panel-default">
        @if(Session::has('Success'))
            <div class="alert alert-success" role="alert">
                {{--{{Session::get('Success')}}--}}
            </div>
        @endif
        <table class="table table-hover">
            <tr>
                <thead>
                <th>
                    Назва продукту
                </th>
                <th>
                    Ціна
                </th>
                <th>
                    Категорія
                </th>
                <th>
                    Створений
                </th>

                <th>
                    Редагувати продукт
                </th>
                <th>
                    Видалити продукт
                </th>
                </thead>
            </tr>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->category_name}}</td>
                    <td>{{\Carbon\Carbon::parse($product->created_at)->diffForHumans()}}</td>
                    <td><a href="{{route('product.edit', ['id'=>$product->id])}}" class="btn btn-info">Редагувати</a></td>
                    <td><a href="{{route('product.delete', ['id'=>$product->id])}}" class="btn btn-danger">У смітник!</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
