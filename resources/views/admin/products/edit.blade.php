@extends ('layouts.app')
@section('content')
    @include('admin.includes.errors')
    <div class="panel panel-default">
        @if(Session::has('Success'))
            <div class="alert alert-success" role="alert">
                {{--{{Session::get('Success')}}--}}
            </div>
        @endif
        <div class="panel-heading">
            Редагувати продукт:<strong>{{$product->name}}</strong>
        </div>
        <div class="panel-body">
            <form action="{{route('product.update', ['id'=>$product->id])}}" method="post">
                {{csrf_field ()}}
                <div class="form-group">
                    <label for="name">Назва</label>
                    <input type="text" name="name" class="form-control" value="{{$product->name}}">
                </div>
                <div class="form-group">
                    <label for="name">Ціна</label>
                    <input type="text" name="price" class="form-control" value="{{$product->price}}">
                </div>
                <div class="form-group">
                    <label for="category">Виберіть категорію</label>
                    <select name="category_id" id="category" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success">
                            Оновити
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
