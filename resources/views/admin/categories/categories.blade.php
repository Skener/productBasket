@extends ('layouts.app')
@section('content')
    @if(count ($errors) > 0 )
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{$error}}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        @if(Session::has('Success'))
            <div class="alert alert-success" role="alert">
                {{Session::get('Success')}}
            </div>
        @endif
        <div class="panel-heading">
            Створити нову категорію
        </div>
        <div class="panel-body">
            <form action="{{route('category.store')}}" method="post">
                {{csrf_field ()}}
                <div class="form-group">
                    <label for="category_name">Назва категорії</label>
                    <input type="text" name="category_name" class="form-control">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">
                            Зберегти категорію
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
