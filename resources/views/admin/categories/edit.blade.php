@extends ('layouts.app')
@section('content')

    @include('admin.includes.errors')
    <div class="panel panel-default">
        @if(Session::has('Success'))
            <div class="alert alert-success" role="alert">
                {{Session::get('Success')}}
            </div>
        @endif
        <div class="panel-heading">
            Редагувати категорію: {{$category->category_name}}
        </div>
        <div class="panel-body">
            <form action="{{route('category.update',['id'=>$category->id])}}" method="post">
                {{csrf_field ()}}
                @method('PUT')
                <div class="form-group">
                    <label for="title">Заголовок</label>
                    <input type="text" name="category_name" value="{{$category->category_name}}" class="form-control">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success">
                            Зберегти
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
