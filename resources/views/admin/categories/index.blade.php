@extends ('layouts.app')
@section('content')
    <div class="panel panel-default">
    <table class="table table-hover">
        <tr>
            <thead>
            <th>
                Назва категорії
            </th>
            <th>
                Редагувати категорію
            </th>
            <th>
                Видалити категорію
            </th>
            </thead>
        </tr>
        <tbody>
        @foreach ($categories as $category)
            <tr>
                <td>
                    {{$category->category_name}}
                </td>
                <td>
                    <a href="{{route('category.edit', ['id'=>$category->id])}}" class="btn btn-xs btn-info">
                        <span class="glyphicon glyphicon-pencil">Edit</span>
                    </a>
                </td>
                <td>
                    <a href="{{route('category.delete', ['id'=>$category->id])}}" class="btn btn-xs btn-danger">
                        <span class="glyphicon glyphicon">X</span>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
    </div>
@endsection
