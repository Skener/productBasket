<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('frontPage');
Route::get('/home', 'HomeController@index')->name('home');
Route::post ( '/order/store', [
    'uses' => 'OrderController@store',
    'as'   => 'order.store'
] );


Route::get ( '/category/products', [
    'uses' => 'PageController@category_products',
    'as'   => 'categories.products'
] );


Route::get ( '/order/view', [
    'uses' => 'OrderController@view',
    'as'   => 'order.view'
] );

Route::group ( [ 'prefix' => 'admin', 'middleware' => 'admin' ], function () {

    Route::get ( '/product/create', [
        'uses' => 'Admin\ProductController@create',
        'as'   => 'product.create'
    ] );

    Route::post ( '/products/store', [
        'uses' => 'Admin\ProductController@store',
        'as'   => 'product.store'
    ] );


    Route::get ( '/products', [
        'uses' => 'Admin\ProductController@index',
        'as'   => 'products'
    ] );
    Route::get ( '/product/delete/{id}', [
        'uses' => 'Admin\ProductController@destroy',
        'as'   => 'product.delete'
    ] );

    Route::get ( '/products/edit/{id}', [
        'uses' => 'Admin\ProductController@edit',
        'as'   => 'product.edit'
    ] );

    Route::put ( '/product/update/{id}', [
        'uses' => 'Admin\ProductController@update',
        'as'   => 'product.update'
    ] );

    Route::get ( '/categories', [
        'uses' => 'Admin\CategoryController@index',
        'as'   => 'categories'
    ] );

    Route::get ( '/category/create', [
        'uses' => 'Admin\CategoryController@create',
        'as'   => 'category.create'
    ] );

    Route::post ( '/category/store', [
        'uses' => 'Admin\CategoryController@store',
        'as'   => 'category.store'
    ] );

    Route::get ( '/category/edit/{id}', [
        'uses' => 'Admin\CategoryController@edit',
        'as'   => 'category.edit'
    ] );

    Route::get ( '/category/delete/{id}', [
        'uses' => 'Admin\CategoryController@delete',
        'as'   => 'category.delete'
    ] );

    Route::post ( '/category/update/{id}', [
        'uses' => 'Admin\CategoryController@update',
        'as'   => 'category.update'
    ] );

} );

Auth::routes();
